public class IPAddressTime {
    private String address;
    private long time;

    IPAddressTime(String address) {
        this.address = address;
        setUpdatedTime();
    }

    public String getAddress() { return address; }

    public long getTime() { return time; }

    public void setUpdatedTime() {time = System.currentTimeMillis(); }

    public boolean equals(Object obj) {
        return ((obj instanceof IPAddressTime) && this.address.equals(((IPAddressTime) obj).address));
    }
  /*  operator == (obj) {
        return this.address == obj.address;
    }*/
}
