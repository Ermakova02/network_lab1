import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class Receiver implements Runnable {
    private static final int RECEIVE_DELAY = 1500;
    private static final int PRESENCE_DELAY = 3000;
    private static final int PACKET_MAX_SIZE = 1024;
    private Thread t;
    private String groupName;
    private int port;
    private boolean stopped;
    private boolean forceRedraw;
    private List<IPAddressTime> list;

    Receiver(String groupName, int port) {
        this.groupName = groupName;
        this.port = port;
        stopped = false;
        forceRedraw = true;
        list = new ArrayList<>();
        t = new Thread(this, "TaskCount: Receiver");
        t.start();
    }

    private void redraw() {
        System.out.println("Run programs:");
        for (int i = 0; i < list.size(); i++)
            System.out.println(list.get(i).getAddress());
    }

    private void updateIPAddress(String address) {
        IPAddressTime ip = new IPAddressTime(address);
        if (list.contains(ip)) {
            list.get(list.indexOf(ip)).setUpdatedTime();
        } else {
            list.add(ip);
            forceRedraw = true;
        }
    }

    private void updateIPAddressesList() {
        long time = System.currentTimeMillis();
        for (int i = 0; i < list.size(); i++) {
            if (time - list.get(i).getTime() > PRESENCE_DELAY) {
                list.remove(list.get(i));
                forceRedraw = true;
            }
        }
        if (forceRedraw) redraw();
    }

    @Override
    public void run() {
        MulticastSocket socket = null;
        InetAddress groupAddress = null;
        try {
            socket = new MulticastSocket(port);
            groupAddress = InetAddress.getByName(groupName);
            socket.joinGroup(groupAddress);
            socket.setSoTimeout(RECEIVE_DELAY);
            DatagramPacket packet = new DatagramPacket(new byte[PACKET_MAX_SIZE], PACKET_MAX_SIZE);
            forceRedraw = true;
            while (!stopped) {
                socket.receive(packet);
                String msg = new String(packet.getData(), packet.getOffset(), packet.getLength());
                if (msg.equals(TaskCount.MESSAGE_TEXT))
                    updateIPAddress(packet.getAddress().toString());
                updateIPAddressesList();
                forceRedraw = false;
            }
            socket.leaveGroup(groupAddress);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        socket.close();
    }
}
