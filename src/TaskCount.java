public class TaskCount {
    public static final String MESSAGE_TEXT = "Message from Task Count";
    private static final String DEFAULT_GROUP = "230.0.0.1";
    private static final int DEFAULT_PORT = 12345;

    public static void main(String[] args) {
        String groupName = DEFAULT_GROUP;
        int port = DEFAULT_PORT;
        switch (args.length) {
            case 1: {
                groupName = args[0];
            } break;
            case 2: {
                groupName = args[0];
                port = Integer.valueOf(args[1]);
            } break;
        }
	    new Sender(groupName, port);
        new Receiver(groupName, port);
    }
}
