import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;

public class Sender implements Runnable {
    private static final int SEND_FREQ = 500;

    private Thread t;
    private String groupName;
    private int port;
    private boolean stopped;

    Sender(String groupName, int port) {
        this.groupName = groupName;
        this.port = port;
        stopped = false;
        t = new Thread(this, "TaskCount: Sender");
        t.start();
    }
    @Override
    public void run() {
        MulticastSocket socket = null;
        InetAddress groupAddress = null;
        try {
            socket = new MulticastSocket(port);
            groupAddress = InetAddress.getByName(groupName);
            socket.joinGroup(groupAddress);
            byte[] msg = TaskCount.MESSAGE_TEXT.getBytes();
            DatagramPacket packet = new DatagramPacket(msg, msg.length);
            packet.setAddress(groupAddress);
            packet.setPort(port);
            while (!stopped) {
                socket.send(packet);
                Thread.sleep(SEND_FREQ);
            }
            socket.leaveGroup(groupAddress);
            socket.close();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
